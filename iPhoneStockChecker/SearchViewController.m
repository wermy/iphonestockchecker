//
//  ViewController.m
//  iPhoneStockChecker
//
//  Created by Warner Skoch on 10/10/14.
//  Copyright (c) 2014 Warner Skoch. All rights reserved.
//

#import "SearchViewController.h"
#import "StoreTableViewCell.h"
#import "DetailsViewController.h"

@interface SearchViewController ()<UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>
@property (nonatomic, strong) NSMutableArray *results;
@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (nonatomic, strong) IBOutlet UITextField *zipField;
@property (nonatomic, strong) NSArray *deviceInfo;
@property (nonatomic, strong) NSDictionary *modelNumbersSixPlus;
@property (nonatomic, strong) NSDictionary *modelNumbersSix;
@property (nonatomic, strong) IBOutlet UISegmentedControl *phoneModel;
@property (nonatomic, strong) IBOutlet UISegmentedControl *color;
@property (nonatomic, strong) IBOutlet UISegmentedControl *capacity;
@property (nonatomic, strong) IBOutlet UISegmentedControl *carrier;
@property (nonatomic, strong) IBOutlet UIView *vBlocker;
@end

@implementation SearchViewController

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self.zipField resignFirstResponder];
    return NO;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 68.0f;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.results.count == 0 ? 1 : self.results.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    StoreTableViewCell *cell = (StoreTableViewCell *)[self.tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (self.results.count == 0) {
        [cell setupWithDict:nil];
    }else{
        [cell setupWithDict:self.results[indexPath.row]];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.row < self.results.count) {
        DetailsViewController *details = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"DetailsViewController"];
        [details setStoreInfo:self.results[indexPath.row]];
        [self.navigationController pushViewController:details animated:YES];
    }
}

- (NSString *)selectedModel{
    NSString *key = @"";
    switch (self.phoneModel.selectedSegmentIndex) {
        case 0:
            key = [key stringByAppendingString:@"six"];
            break;
        case 1:
            key = [key stringByAppendingString:@"six_plus"];
            break;
        default:
            break;
    }
    
    switch (self.color.selectedSegmentIndex) {
        case 0:
            key = [key stringByAppendingString:@"-silver"];
            break;
        case 1:
            key = [key stringByAppendingString:@"-gold"];
            break;
        case 2:
            key = [key stringByAppendingString:@"-gray"];
            break;
        default:
            break;
    }
    
    switch (self.capacity.selectedSegmentIndex) {
        case 0:
            key = [key stringByAppendingString:@"-16gb"];
            break;
        case 1:
            key = [key stringByAppendingString:@"-64gb"];
            break;
        case 2:
            key = [key stringByAppendingString:@"-128gb"];
            break;
        default:
            break;
    }
    
    key = [key stringByAppendingString:@"-"];
    key = [key stringByAppendingString:[self.carrier titleForSegmentAtIndex:[self.carrier selectedSegmentIndex]]];
    NSLog(@"%@", key);
    NSDictionary *deviceInfo = self.deviceInfo[self.phoneModel.selectedSegmentIndex];
    NSString *ret = [deviceInfo[key] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    return ret;
}

- (IBAction)fetchResults:(id)sender{
    [self.zipField resignFirstResponder];
    if (self.zipField.text.length == 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Enter a zip code." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    [[NSUserDefaults standardUserDefaults] setObject:self.zipField.text forKey:@"storedZip"];
    [[NSUserDefaults standardUserDefaults] setObject:@(self.phoneModel.selectedSegmentIndex) forKey:@"phoneModel"];
    [[NSUserDefaults standardUserDefaults] setObject:@(self.color.selectedSegmentIndex) forKey:@"color"];
    [[NSUserDefaults standardUserDefaults] setObject:@(self.capacity.selectedSegmentIndex) forKey:@"capacity"];
    [[NSUserDefaults standardUserDefaults] setObject:@(self.carrier.selectedSegmentIndex) forKey:@"carrier"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://store.apple.com/us/retailStore/availabilitySearch?parts.0=%@&zip=%@", [self selectedModel], self.zipField.text]];
    NSMutableURLRequest *req = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:30];
    __weak SearchViewController *weakSelf = self;
    [self.vBlocker setHidden:NO];
    [NSURLConnection sendAsynchronousRequest:req
                                       queue:[[NSOperationQueue alloc] init]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               dispatch_async(dispatch_get_main_queue(), ^{
                                   [weakSelf.vBlocker setHidden:YES];
                               });
                               
                               if (error) {
                                   
                               } else {
                                   NSError *localError = nil;
                                   NSString *jsonString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                                   NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:[jsonString dataUsingEncoding:NSUTF8StringEncoding]
                                                                                                options:0
                                                                                                  error:&localError];
                                   if (localError) {
                                       
                                   } else {
                                       self.results = [[parsedObject objectForKey:@"body"] objectForKey:@"stores"];
                                       NSLog(@"%@", self.results);
                                       dispatch_async(dispatch_get_main_queue(), ^{
                                           [self.tableView setContentOffset:CGPointZero animated:NO];
                                           [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationMiddle];
                                       });
                                   }
                               }
                           }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setAutomaticallyAdjustsScrollViewInsets:NO];
    [self.vBlocker setHidden:YES];
    self.modelNumbersSixPlus = @{@"six_plus-gray-16gb-AT&T" : @"MGAL2LL/A",
                                 @"six_plus-silver-16gb-AT&T" : @"MGAM2LL/A",
                                 @"six_plus-gold-16gb-AT&T" : @"MGAN2LL/A",
                                 @"six_plus-gray-64gb-AT&T" : @"MGAU2LL/A",
                                 @"six_plus-silver-64gb-AT&T" : @"MGAV2LL/A",
                                 @"six_plus-gold-64gb-AT&T" : @"MGAW2LL/A",
                                 @"six_plus-gray-128gb-AT&T" : @"MGAP2LL/A",
                                 @"six_plus-silver-128gb-AT&T" : @"MGAQ2LL/A",
                                 @"six_plus-gold-128gb-AT&T" : @"MGAR2LL/A",
                                 
                                 @"six_plus-gray-16gb-Sprint" : @"MGCV2LL/A",
                                 @"six_plus-silver-16gb-Sprint" : @"MGCW2LL/A",
                                 @"six_plus-gold-16gb-Sprint" : @"MGCX2LL/A",
                                 @"six_plus-gray-64gb-Sprint" : @"MGD22LL/A",
                                 @"six_plus-silver-64gb-Sprint" : @"MGD32LL/A",
                                 @"six_plus-gold-64gb-Sprint" : @"MGD42LL/A",
                                 @"six_plus-gray-128gb-Sprint" : @"MGCY2LL/A",
                                 @"six_plus-silver-128gb-Sprint" : @"MGD02LL/A",
                                 @"six_plus-gold-128gb-Sprint" : @"MGD12LL/A",
                                 
                                 @"six_plus-gray-16gb-Verizon" : @"MGCK2LL/A",
                                 @"six_plus-silver-16gb-Verizon" : @"MGCL2LL/A",
                                 @"six_plus-gold-16gb-Verizon" : @"MGCM2LL/A",
                                 @"six_plus-gray-64gb-Verizon" : @"MGCR2LL/A",
                                 @"six_plus-silver-64gb-Verizon" : @"MGCT2LL/A",
                                 @"six_plus-gold-64gb-Verizon" : @"MGCU2LL/A",
                                 @"six_plus-gray-128gb-Verizon" : @"MGCN2LL/A",
                                 @"six_plus-silver-128gb-Verizon" : @"MGCP2LL/A",
                                 @"six_plus-gold-128gb-Verizon" : @"MGCQ2LL/A",
                                 
                                 @"six_plus-gray-16gb-T-Mobile" : @"MGAX2LL/A",
                                 @"six_plus-silver-16gb-T-Mobile" : @"MGC02LL/A",
                                 @"six_plus-gold-16gb-T-Mobile" : @"MGC12LL/A",
                                 @"six_plus-gray-64gb-T-Mobile" : @"MGC52LL/A",
                                 @"six_plus-silver-64gb-T-Mobile" : @"MGC62LL/A",
                                 @"six_plus-gold-64gb-T-Mobile" : @"MGC72LL/A",
                                 @"six_plus-gray-128gb-T-Mobile" : @"MGC22LL/A",
                                 @"six_plus-silver-128gb-T-Mobile" : @"MGC32LL/A",
                                 @"six_plus-gold-128gb-T-Mobile" : @"MGC42LL/A"};
    
    self.modelNumbersSix = @{@"six-gray-16gb-AT&T" : @"MG4N2LL/A",
                             @"six-silver-16gb-AT&T" : @"MG4P2LL/A",
                             @"six-gold-16gb-AT&T" : @"MG4Q2LL/A",
                             @"six-gray-64gb-AT&T" : @"MG4W2LL/A",
                             @"six-silver-64gb-AT&T" : @"MG4X2LL/A",
                             @"six-gold-64gb-AT&T" : @"MG502LL/A",
                             @"six-gray-128gb-AT&T" : @"MG4R2LL/A",
                             @"six-silver-128gb-AT&T" : @"MG4U2LL/A",
                             @"six-gold-128gb-AT&T" : @"MG4V2LL/A",
                             
                             @"six-gray-16gb-Sprint" : @"MG692LL/A",
                             @"six-silver-16gb-Sprint" : @"MG6A2LL/A",
                             @"six-gold-16gb-Sprint" : @"MG6C2LL/A",
                             @"six-gray-64gb-Sprint" : @"MG6G2LL/A",
                             @"six-silver-64gb-Sprint" : @"MG6H2LL/A",
                             @"six-gold-64gb-Sprint" : @"MG6J2LL/A",
                             @"six-gray-128gb-Sprint" : @"MG6D2LL/A",
                             @"six-silver-128gb-Sprint" : @"MG6E2LL/A",
                             @"six-gold-128gb-Sprint" : @"MG6F2LL/A",
                             
                             @"six-gray-16gb-Verizon" : @"MG5W2LL/A",
                             @"six-silver-16gb-Verizon" : @"MG5X2LL/A",
                             @"six-gold-16gb-Verizon" : @"MG5Y2LL/A",
                             @"six-gray-64gb-Verizon" : @"MG632LL/A",
                             @"six-silver-64gb-Verizon" : @"MG642LL/A",
                             @"six-gold-64gb-Verizon" : @"MG652LL/A",
                             @"six-gray-128gb-Verizon" : @"MG602LL/A",
                             @"six-silver-128gb-Verizon" : @"MG612LL/A",
                             @"six-gold-128gb-Verizon" : @"MG622LL/A",
                             
                             @"six-gray-16gb-T-Mobile" : @"MG542LL/A",
                             @"six-silver-16gb-T-Mobile" : @"MG552LL/A",
                             @"six-gold-16gb-T-Mobile" : @"MG562LL/A",
                             @"six-gray-64gb-T-Mobile" : @"MG5A2LL/A",
                             @"six-silver-64gb-T-Mobile" : @"MG5C2LL/A",
                             @"six-gold-64gb-T-Mobile" : @"MG5D2LL/A",
                             @"six-gray-128gb-T-Mobile" : @"MG572LL/A",
                             @"six-silver-128gb-T-Mobile" : @"MG582LL/A",
                             @"six-gold-128gb-T-Mobile" : @"MG592LL/A"};
    
    self.deviceInfo = @[self.modelNumbersSix, self.modelNumbersSixPlus];
    
    [self.zipField setText:[[NSUserDefaults standardUserDefaults] objectForKey:@"storedZip"]];
    [self.phoneModel setSelectedSegmentIndex:[[[NSUserDefaults standardUserDefaults] objectForKey:@"phoneModel"] integerValue]];
    [self.color setSelectedSegmentIndex:[[[NSUserDefaults standardUserDefaults] objectForKey:@"color"] integerValue]];
    [self.capacity setSelectedSegmentIndex:[[[NSUserDefaults standardUserDefaults] objectForKey:@"capacity"] integerValue]];
    [self.carrier setSelectedSegmentIndex:[[[NSUserDefaults standardUserDefaults] objectForKey:@"carrier"] integerValue]];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"StoreTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"cell"];
    if (self.zipField.text.length > 0) {
        [self fetchResults:nil];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
