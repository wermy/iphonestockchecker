//
//  StoreCellTableViewCell.h
//  iPhoneStockChecker
//
//  Created by Warner Skoch on 10/10/14.
//  Copyright (c) 2014 Warner Skoch. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StoreTableViewCell : UITableViewCell
- (void)setupWithDict:(NSDictionary *)dict;
@end
