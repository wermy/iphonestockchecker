//
//  DetailsViewController.h
//  iPhoneStockChecker
//
//  Created by Warner Skoch on 10/15/14.
//  Copyright (c) 2014 Warner Skoch. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailsViewController : UIViewController
@property (nonatomic, strong) NSDictionary *storeInfo;
@property (nonatomic, strong) IBOutlet UITextView *infoText;
@property (nonatomic, strong) IBOutlet UIImageView *storeImage;
@end
