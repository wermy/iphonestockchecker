//
//  DetailsViewController.m
//  iPhoneStockChecker
//
//  Created by Warner Skoch on 10/15/14.
//  Copyright (c) 2014 Warner Skoch. All rights reserved.
//

#import "DetailsViewController.h"

@interface DetailsViewController ()

@end

@implementation DetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if (self.storeInfo) {
        NSString *info = @"";
        if (self.storeInfo[@"storeDisplayName"]) {
            info = [info stringByAppendingString:self.storeInfo[@"storeDisplayName"]];
            info = [info stringByAppendingString:@"\n"];
        }
        NSDictionary *address = self.storeInfo[@"address"];
        if (address && self.storeInfo[@"address"] != [NSNull null]) {
            if (address[@"address"] && address[@"address"] != [NSNull null]) {
                info = [info stringByAppendingString:address[@"address"]];
                info = [info stringByAppendingString:@"\n"];
            }
            if (address[@"address2"] && address[@"address2"] != [NSNull null]) {
                info = [info stringByAppendingString:address[@"address2"]];
                info = [info stringByAppendingString:@"\n"];
            }
            if (address[@"address3"] && address[@"address3"] != [NSNull null]) {
                info = [info stringByAppendingString:address[@"address3"]];
                info = [info stringByAppendingString:@"\n"];
            }
        }
        
        if (self.storeInfo[@"city"] && self.storeInfo[@"city"] != [NSNull null]) {
            info = [info stringByAppendingString:self.storeInfo[@"city"]];
            info = [info stringByAppendingString:@", "];
        }
        
        if (self.storeInfo[@"state"] && self.storeInfo[@"state"] != [NSNull null]) {
            info = [info stringByAppendingString:self.storeInfo[@"state"]];
            info = [info stringByAppendingString:@" "];
        }
        
        if (address && self.storeInfo[@"address"] != [NSNull null]) {
            if (address[@"postalCode"] && address[@"postalCode"] != [NSNull null]) {
                info = [info stringByAppendingString:address[@"postalCode"]];
                info = [info stringByAppendingString:@"\n"];
            }
        }
        
        if (self.storeInfo[@"phoneNumber"] && self.storeInfo[@"phoneNumber"] != [NSNull null]) {
            info = [info stringByAppendingString:@"\n"];
            info = [info stringByAppendingString:self.storeInfo[@"phoneNumber"]];
            info = [info stringByAppendingString:@"\n"];
        }
        
        if (self.storeInfo[@"storeHours"] && self.storeInfo[@"storeHours"] != [NSNull null]) {
            NSDictionary *storeHours = self.storeInfo[@"storeHours"];
            NSArray *hours = storeHours[@"hours"];
            if (hours && storeHours[@"hours"] != [NSNull null]) {
                info = [info stringByAppendingString:@"\n"];
                for (NSDictionary *hour in hours) {
                    info = [info stringByAppendingString:hour[@"storeDays"]];
                    info = [info stringByAppendingString:@" "];
                    info = [info stringByAppendingString:hour[@"storeTimings"]];
                    info = [info stringByAppendingString:@"\n"];
                }
            }
        }
        
        [self.infoText setText:info];
    }
    
    NSString *imageUrl = self.storeInfo[@"storeImageUrl"];
    if (imageUrl && self.storeInfo[@"storeImageUrl"] != [NSNull null]) {
        __weak DetailsViewController *weakSelf = self;
        NSMutableURLRequest *req = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:imageUrl] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:30];
        [NSURLConnection sendAsynchronousRequest:req
                                           queue:[[NSOperationQueue alloc] init]
                               completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                                   
                                   if (error) {
                                       
                                   } else {
                                       UIImage *img = [[UIImage alloc] initWithData:data];
                                       dispatch_async(dispatch_get_main_queue(), ^{
                                           [weakSelf.storeImage setImage:img];
                                       });
                                   }
                               }];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
