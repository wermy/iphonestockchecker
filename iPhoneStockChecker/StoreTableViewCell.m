//
//  StoreCellTableViewCell.m
//  iPhoneStockChecker
//
//  Created by Warner Skoch on 10/10/14.
//  Copyright (c) 2014 Warner Skoch. All rights reserved.
//

#import "StoreTableViewCell.h"

@interface StoreTableViewCell ()
@property (nonatomic, strong) IBOutlet UILabel *storeName;
@property (nonatomic, strong) IBOutlet UILabel *stockStatus;
@end

@implementation StoreTableViewCell

- (void)setupWithDict:(NSDictionary *)dict{
    if (dict) {
        [self.storeName setText:dict[@"storeDisplayName"]];
        
        NSDictionary *partsAvailability = [dict objectForKey:@"partsAvailability"];
        NSString *availableString = [[partsAvailability objectForKey:partsAvailability.allKeys.firstObject] objectForKey:@"pickupDisplay"];
        if ([availableString isEqualToString:@"available"]) {
            [self.stockStatus setText:@"In stock!"];
            [self setBackgroundColor:[UIColor colorWithRed:151.0f/255.0f green:255.0f/255.0f blue:137.0f/255.0f alpha:1.0f]];
        }else{
            [self.stockStatus setText:[availableString capitalizedString]];
            [self setBackgroundColor:[UIColor colorWithRed:255.0f/255.0f green:146.0f/255.0f blue:123.0f/255.0f alpha:1.0f]];
        }
    }else{
        [self.storeName setText:@"No stores found."];
        [self.stockStatus setText:@""];
    }
}

@end
